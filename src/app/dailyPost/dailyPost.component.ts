import {Component, CORE_DIRECTIVES, FORM_DIRECTIVES, OnInit}    from 'angular2/angular2';
import {Router}                                                 from 'angular2/router';
import {ROUTE_NAMES}                                            from './../routes';

@Component({
    selector: 'my-dailyPost',
	templateUrl: 'app/dailyPost/dailyPost.component.html',
	styleUrls: ['app/dailyPost/dailyPost.component.css'],
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES]
})

export class DailyPostComponent implements OnInit {

    ngOnInit() { console.log('Daily Post Loaded'); }

}
