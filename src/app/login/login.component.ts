import {Component, FORM_DIRECTIVES}                          from 'angular2/angular2';

@Component({
    selector:       'my-login',
    templateUrl:    'app/login/login.component.html',
    styleUrls:      ['app/login/login.component.css'],
    directives:     [FORM_DIRECTIVES]
})

export class LoginComponent {

}