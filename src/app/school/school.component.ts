import {Component, CORE_DIRECTIVES, FORM_DIRECTIVES, OnInit}    from 'angular2/angular2';
import {Router}                                                 from 'angular2/router';
import {ROUTE_NAMES}                                            from './../routes';

@Component({
    selector: 'my-school',
	templateUrl: 'app/school/school.component.html',
	styleUrls: ['app/school/school.component.css'],
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES]
})

export class SchoolComponent implements OnInit {

    ngOnInit() { console.log('School Loaded'); }

}
