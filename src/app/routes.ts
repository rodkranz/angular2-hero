import {RouteDefinition}            from 'angular2/router';
import {DashboardComponent}         from './dashboard/dashboard.component';
import {SchoolComponent}            from './school/school.component';
import {DailyPostComponent}         from './dailyPost/dailyPost.component';
import {LoginComponent}             from './login/login.component';

export const ROUTE_NAMES = {
    dashboard:  'Dashboard',
    dailyPost:  'DailyPost',
    school:     'School',
    login:      'Login'
};

export const ROUTES: RouteDefinition[] = [
    { path: '/', redirectTo: [ROUTE_NAMES.dashboard] },
    { path: '/dashboard',   name: ROUTE_NAMES.dashboard,  component: DashboardComponent	},
    { path: '/schools',     name: ROUTE_NAMES.school,     component: SchoolComponent    },
    { path: '/dailyPost',   name: ROUTE_NAMES.dailyPost,  component: DailyPostComponent},
    { path: '/login',       name: ROUTE_NAMES.login,      component: LoginComponent }
];
