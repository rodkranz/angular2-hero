import {Component, CORE_DIRECTIVES, FORM_DIRECTIVES, OnInit}    from 'angular2/angular2';
import {Router}                                                 from 'angular2/router';
import {ROUTE_NAMES}                                            from './../routes';

@Component({
    selector: 'my-dashboard',
	templateUrl: 'app/dashboard/dashboard.component.html',
	styleUrls: ['app/dashboard/dashboard.component.css'],
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES]
})

export class DashboardComponent implements OnInit {

    ngOnInit() { console.log('Oi'); }

}
