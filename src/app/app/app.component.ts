import {Component}                                  from 'angular2/angular2';
import {RouteConfig, ROUTER_DIRECTIVES}             from 'angular2/router';
import {NavbarComponent}                            from './navbar/navbar.component';
import {ROUTES}                                     from './../routes';

@Component({
    selector:       'taba-app',
    templateUrl:    'app/app/app.component.html',
    styleUrls:      ['app/app/app.component.css'],
    directives: [ROUTER_DIRECTIVES, NavbarComponent]
})

@RouteConfig(ROUTES)
export class AppComponent {
}

