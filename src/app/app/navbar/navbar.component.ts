import {Component, OnInit}                          from 'angular2/angular2';
import {ROUTE_NAMES}                                from './../../routes';
import {RouteConfig, ROUTER_DIRECTIVES}             from 'angular2/router';

@Component({
    selector:       'my-navbar',
    templateUrl:    'app/app/navbar/navbar.component.html',
    // styleUrls:      ['app/app/app.component.css'],
    directives:     [ROUTER_DIRECTIVES]
})

export class NavbarComponent {
    public routes = [];
    public menuTitle = "Angular2";
    ngOnInit() {
        for (var n in ROUTE_NAMES) {
            this.routes.push({
                'title': n,
                'url'  : ROUTE_NAMES[n]
            });
        }
    }
}
